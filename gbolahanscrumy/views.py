from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.db import models
from django.contrib.auth.models import User
from gbolahanscrumy.models import *
from django.http import HttpResponse
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.http import HttpResponseRedirect
from django.contrib.auth.models import Group
from django.contrib.auth.forms import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from .forms import CustomUserCreationForm
from django.contrib import messages
import random

def index1(request):
    return HttpResponse("This is a Scrum Application")

# def index(request, *args, **kwargs):    
  #  qs = ScrumyGoals.objects.filter(goal_name="Learn Django").get()
   #  return HttpResponse(f'{qs}')

def home(request):
    users = User.objects.all()

    def get_by_status(status_name):
        goals = GoalStatus.objects.get(status_name=status_name)
        status_goals = goals.scrumygoals_set.all()
        return status_goals

    daily_goals = get_by_status("Daily Goal")
    weekly_goals = get_by_status("Weekly Goal")
    verify_goals = get_by_status("Verify Goal")
    done_goals = get_by_status("Done Goal")

    dictionary = {
        'users': users,
        'weekly_goals': weekly_goals,
        'daily_goals': daily_goals,
        'verify_goals': verify_goals,
        'done_goals': done_goals,
    }
    return render(request, 'gbolahanscrumy/home.html', dictionary)
def move_goal(request, goal_id):
    try:
        obj = ScrumyGoals.objects.get(goal_id=goal_id)
    except Exception as e:
        return render(request, 'exception.html',
        {'error':'A record with that goal id does not exist'})
    else:
        return HttpResponse(obj.goal_name)

@login_required(login_url="/gbolahanscrumy/accounts/login")

def sign_up(request):
    f = CustomUserCreationForm(request.POST)
    if f.is_valid():
        f.save()
        messages.success(request, 'Account created successfully')
        response = redirect('successpage/')
        return response
    else:
        f = CustomUserCreationForm()
 
    return render(request, 'registration/signup.html', {'form': f})

def add_goal(request):
    form = CreateGoalForm
    if request.method == 'POST':
        form = form(request.POST)
        data = request.POST.dict()
        user = User.objects.get(id=data['user'])
        add_goal = form.save(commit=False)
        add_goal.goal_id = random.randint(1000,9990)
        add_goal.created_by = user.username
        add_goal.moved_by = user.username
        add_goal.owner = user.username
        status = GoalStatus.objects.get(status_name="Daily Goal")
        add_goal.goal_status = status
        add_goal.save()
        return HttpResponseRedirect('/gbolahanscrumy/home' )
    context = {'create_goal': form }
    
    return render(request, 'gbolahanscrumy/addgoal.html', context)




