from django.urls import path
from . import views
from django.contrib.auth import urls
from django.urls import path, include
from django.contrib.auth.models import *
from django.conf.urls import url
from django.views.generic import TemplateView
from django.contrib.auth.forms import UserCreationForm
from .views import *
app_name = 'gbolahanscrumy'
urlpatterns = [
path('', sign_up, name="signup"),
path('movegoal/<int:goal_id>', views.move_goal),
path('addgoal', views.add_goal),
path('home/', home, name='home'),
path('accounts/', include('django.contrib.auth.urls')),
path('accounts/signup/', sign_up, name='signup'),
path('addgoal/', add_goal, name='add'),
path('successpage/', TemplateView.as_view(
        template_name='gbolahanscrumy/successpage.html'), name='successpage'),
]










